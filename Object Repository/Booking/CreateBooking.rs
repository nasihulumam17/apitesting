<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>CreateBooking</name>
   <tag></tag>
   <elementGuidId>3405901d-7209-4ccb-b1d4-fd0bbda0e749</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;firstname\&quot; : \&quot;${firstname}\&quot;,\n    \&quot;lastname\&quot; : \&quot;${lastname}\&quot;,\n    \&quot;totalprice\&quot; : ${totalprice},\n    \&quot;depositpaid\&quot; : \&quot;${depositpaid}\&quot;,\n    \&quot;bookingdates\&quot; : {\n        \&quot;checkin\&quot; : \&quot;${checkin}\&quot;,\n        \&quot;checkout\&quot; : \&quot;${checkout}\&quot;\n    },\n    \&quot;additionalneeds\&quot; : \&quot;${additionalneeds}\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>b9b23284-1c4c-479d-9ea5-49e021296d8d</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>f4002374-8675-456c-8b65-a0bfb14c84c4</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://restful-booker.herokuapp.com/booking</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
def variables = request.getVariables()
def firstname = variables.get('firstname')
def lastname = variables.get('lastname')
def totalprice = variables.get('totalprice')
def depositpaid = variables.get('depositpaid')
def checkin = variables.get('checkin')
def checkout = variables.get('checkout')
def additionalneeds = variables.get('additionalneeds')

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyElementPropertyValue(response, 'booking.firstname', firstname)
WS.verifyElementPropertyValue(response, 'booking.lastname', lastname)
WS.verifyElementPropertyValue(response, 'booking.totalprice', totalprice)
WS.verifyElementPropertyValue(response, 'booking.depositpaid', depositpaid)
WS.verifyElementPropertyValue(response, 'booking.bookingdates.checkin', checkin)
WS.verifyElementPropertyValue(response, 'booking.bookingdates.checkout', checkout)
WS.verifyElementPropertyValue(response, 'booking.additionalneeds', additionalneeds)
assertThat(response.getStatusCode()).isEqualTo(200)

GlobalVariable.bookingId = WS.getElementPropertyValue(response, 'bookingid')
GlobalVariable.firstname = WS.getElementPropertyValue(response, 'booking.firstname')
GlobalVariable.lastname = WS.getElementPropertyValue(response, 'booking.lastname')
GlobalVariable.totalprice = WS.getElementPropertyValue(response, 'booking.totalprice')
GlobalVariable.depositpaid = WS.getElementPropertyValue(response, 'booking.depositpaid')
GlobalVariable.checkin = WS.getElementPropertyValue(response, 'booking.bookingdates.checkin')
GlobalVariable.checkout = WS.getElementPropertyValue(response, 'booking.bookingdates.checkout')
GlobalVariable.additionalneeds = WS.getElementPropertyValue(response, 'booking.additionalneeds')
System.out.println(GlobalVariable.bookingId)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
